package botay.example.botay_app;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.os.Bundle;
public class ChoiseSubject extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choise_subject);

        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
    public void choiseMatan(View view){
        Intent intent = new Intent(this, DisplayVictorinaActivity.class);
        startActivity(intent);
    }
    public void choiseLinal(View view){
        Intent intent = new Intent(this, LinalVictorina.class);
        startActivity(intent);
    }
    public void choisePhisic(View view){
        Intent intent = new Intent(this, PhisicVictorina.class);
        startActivity(intent);
    }
    public void choiseDiscrMath(View view){
        Intent intent = new Intent(this, DiscrMathVictorina.class);
        startActivity(intent);
    }
}