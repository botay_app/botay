package botay.example.botay_app;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class DiscrMathVictorina extends AppCompatActivity {

    private String[] questions = {"А и В - множества действительных чисел: А = [0,7], B =(2,4]. Множество A\\B равно", "Подстановка значений X = 0 и X = 1 показывает, что булева функция X->1 тождественно равна функции", "Число строк в таблице булевой функции f(X, Y, Z) равно", "Выражение X \\ Y = Z представляет собой", "Число слов длины 2 в алфавите {a, b, c, d, e} равно", "Предикатная формула \n ∀Z P(X, Y, Z) \n представляет собой", "Декартовым произведением АхB множеств А={4,5} и B={2,6} является", "Для истинности сложного высказывания X U Y истинность простого высказывания Y является условием", "Если f(X)=tgX, g(X,Y)=X-Y, \n то суперпозиция f(g(Y, X)) выражает функцию", "Какому определению соответствует формула: \n X & Y & ¬Z U X & Y & Z"};
    private String[] answers1 = {"[0,2] U (4,7]", "ø", "[0,2] U [4,7]", "[0,2] U (4,7]"};
    private String[] answers2 = {"0", "1", "¬X", "X"};
    private String[] answers3 = {"8", "9", "2", "4"};
    private String[] answers4 = {"Трехместную алгебраическую операцию", "Бинарное отношение", "Двуместную алгебраическую операцию", "Трехместное отношение"};
    private String[] answers5 = {"120", "20", "32", "25"};
    private String[] answers6 = {"Истинное высказывание", "Ложное высказывание", "Двуместный предикат", "Логическую константу"};
    private String[] answers7 = {"{8, 10, 24, 30}", "{(4,2), (4,6), (5,2), (5,6)}", "{(2,4), (2,5), (6,4), (6,5)}", "{4*5*2*6}"};
    private String[] answers8 = {"Не является ни необходимым, ни достаточным", "Необходимым, но недостаточным", "Достаточным, но не необходимым", "Необходимым и достаточным"};
    private String[] answers9 = {"tgX-tgY", "tg(X-Y)", "tg(Y-X)", "tgY-tgX"};
    private String[] answers10 = {"ДНФ", "КНФ", "СКНФ", "СДНФ"};
    int q = 1;
    int sum = 0;
    private int mCurrentIndex = 0;
    TextView question;
    Button button1;
    Button button2;
    Button button3;
    Button button4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discr_math_victorina);

        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        question = (TextView) findViewById(R.id.question);
        button1 = (Button) findViewById(R.id.ans1);
        button2 = (Button) findViewById(R.id.ans2);
        button3 = (Button) findViewById(R.id.ans3);
        button4 = (Button) findViewById(R.id.ans4);


        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer1();
                ++mCurrentIndex;
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer2();
                ++mCurrentIndex;
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer3();
                ++mCurrentIndex;
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer4();
                ++mCurrentIndex;
            }
        });
    }
    private void checkAnswer1() {
        switch (q) {
            case 3:
            case 4:
                AlertDialog.Builder b = new AlertDialog.Builder(this);
                b.setTitle("Верно!").setIcon(R.drawable.correct2).setCancelable(false).setPositiveButton("Дальше", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        updateQuestion();
                        updateAnswers();
                    }
                });
                if (q < 10) {
                    AlertDialog al1 = b.create();
                    al1.show();
                }
                ++sum;
                break;
            case 1:
            case 2:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
                AlertDialog.Builder b1 = new AlertDialog.Builder(this);
                b1.setTitle("Неверно!").setIcon(R.drawable.uncorrect2).setCancelable(false).setPositiveButton("Дальше", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        updateQuestion();
                        updateAnswers();
                    }
                });
                if (q < 10) {
                    AlertDialog al1 = b1.create();
                    al1.show();
                }
                break;
        }
        if (q == 10){
            final Intent intent1 = new Intent(this, MainActivity.class);
            AlertDialog.Builder d1 = new AlertDialog.Builder(this);
            d1.setTitle("Количество правильных ответов: " + sum).setCancelable(false).setPositiveButton("Вернуться в меню", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivity(intent1);
                }
            });
            AlertDialog al2 = d1.create();
            al2.show();
        }
        ++q;
    }

    private void checkAnswer2() {
        switch (q) {
            case 2:
            case 7:
                AlertDialog.Builder b = new AlertDialog.Builder(this);
                b.setTitle("Верно!").setIcon(R.drawable.correct2).setCancelable(false).setPositiveButton("Дальше", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        updateQuestion();
                        updateAnswers();
                    }
                });
                if (q < 10) {
                    AlertDialog al1 = b.create();
                    al1.show();
                }
                ++sum;
                break;
            case 1:
            case 3:
            case 4:
            case 5:
            case 6:
            case 8:
            case 9:
            case 10:
                AlertDialog.Builder b1 = new AlertDialog.Builder(this);
                b1.setTitle("Неверно!").setIcon(R.drawable.uncorrect2).setCancelable(false).setPositiveButton("Дальше", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        updateQuestion();
                        updateAnswers();
                    }
                });
                if (q < 10) {
                    AlertDialog al1 = b1.create();
                    al1.show();
                }
                break;
        }
        if (q == 10){
            final Intent intent1 = new Intent(this, MainActivity.class);
            AlertDialog.Builder d1 = new AlertDialog.Builder(this);
            d1.setTitle("Количество правильных ответов: " + sum).setCancelable(false).setPositiveButton("Вернуться в меню", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivity(intent1);
                }
            });
            AlertDialog al2 = d1.create();
            al2.show();
        }
        ++q;
    }
    private void checkAnswer3() {
        switch (q) {
            case 6:
            case 8:
            case 9:
                AlertDialog.Builder b = new AlertDialog.Builder(this);
                b.setTitle("Верно!").setIcon(R.drawable.correct2).setCancelable(false).setPositiveButton("Дальше", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        updateQuestion();
                        updateAnswers();
                    }
                });
                if (q < 10) {
                    AlertDialog al1 = b.create();
                    al1.show();
                }
                ++sum;
                break;
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 7:
            case 10:
                AlertDialog.Builder b1 = new AlertDialog.Builder(this);
                b1.setTitle("Неверно!").setIcon(R.drawable.uncorrect2).setCancelable(false).setPositiveButton("Дальше", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        updateQuestion();
                        updateAnswers();
                    }
                });
                if (q < 10) {
                    AlertDialog al1 = b1.create();
                    al1.show();
                }
                break;
        }
        if (q == 10){
            final Intent intent1 = new Intent(this, MainActivity.class);
            AlertDialog.Builder d1 = new AlertDialog.Builder(this);
            d1.setTitle("Количество правильных ответов: " + sum).setCancelable(false).setPositiveButton("Вернуться в меню", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivity(intent1);
                }
            });
            AlertDialog al2 = d1.create();
            al2.show();
        }
        ++q;
    }


    private void checkAnswer4() {
        switch (q) {
            case 1:
            case 5:
            case 10:
                AlertDialog.Builder b = new AlertDialog.Builder(this);
                b.setTitle("Верно!").setIcon(R.drawable.correct2).setCancelable(false).setPositiveButton("Дальше", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        updateQuestion();
                        updateAnswers();
                    }
                });
                if (q < 10) {
                    AlertDialog al1 = b.create();
                    al1.show();
                }
                ++sum;
                break;
            case 2:
            case 3:
            case 4:
            case 6:
            case 7:
            case 8:
            case 9:
                AlertDialog.Builder b1 = new AlertDialog.Builder(this);
                b1.setTitle("Неверно!").setIcon(R.drawable.uncorrect2).setCancelable(false).setPositiveButton("Дальше", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        updateQuestion();
                        updateAnswers();
                    }
                });
                if (q < 10) {
                    AlertDialog al1 = b1.create();
                    al1.show();
                }
                break;
        }
        if (q == 10){
            final Intent intent1 = new Intent(this, MainActivity.class);
            AlertDialog.Builder d1 = new AlertDialog.Builder(this);
            d1.setTitle("Количество правильных ответов: " + sum).setCancelable(false).setPositiveButton("Вернуться в меню", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivity(intent1);
                }
            });
            AlertDialog al2 = d1.create();
            al2.show();
        }
        ++q;
    }
    private void updateQuestion() {
        String quest = questions[mCurrentIndex];
        question.setText(quest);
    }

    private void updateAnswers() {
        switch (mCurrentIndex){
            case 0:
                String ans1_1 = answers1[0];
                button1.setText(ans1_1);
                String ans1_2 = answers1[1];
                button2.setText(ans1_2);
                String ans1_3 = answers1[2];
                button3.setText(ans1_2);
                String ans1_4 = answers1[3];
                button4.setText(ans1_4);
                return;
            case 1:
                String ans2_1 = answers2[0];
                button1.setText(ans2_1);
                String ans2_2 = answers2[1];
                button2.setText(ans2_2);
                String ans2_3 = answers2[2];
                button3.setText(ans2_3);
                String ans2_4 = answers2[3];
                button4.setText(ans2_4);
                return;
            case 2:
                String ans3_1 = answers3[0];
                button1.setText(ans3_1);
                String ans3_2 = answers3[1];
                button2.setText(ans3_2);
                String ans3_3 = answers3[2];
                button3.setText(ans3_3);
                String ans3_4 = answers3[3];
                button4.setText(ans3_4);
                return;
            case 3:
                String ans4_1 = answers4[0];
                button1.setText(ans4_1);
                String ans4_2 = answers4[1];
                button2.setText(ans4_2);
                String ans4_3 = answers4[2];
                button3.setText(ans4_3);
                String ans4_4 = answers4[3];
                button4.setText(ans4_4);
                return;
            case 4:
                String ans5_1 = answers5[0];
                button1.setText(ans5_1);
                String ans5_2 = answers5[1];
                button2.setText(ans5_2);
                String ans5_3 = answers5[2];
                button3.setText(ans5_3);
                String ans5_4 = answers5[3];
                button4.setText(ans5_4);
                return;
            case 5:
                String ans6_1 = answers6[0];
                button1.setText(ans6_1);
                String ans6_2 = answers6[1];
                button2.setText(ans6_2);
                String ans6_3 = answers6[2];
                button3.setText(ans6_3);
                String ans6_4 = answers6[3];
                button4.setText(ans6_4);
                return;
            case 6:
                String ans7_1 = answers7[0];
                button1.setText(ans7_1);
                String ans7_2 = answers7[1];
                button2.setText(ans7_2);
                String ans7_3 = answers7[2];
                button3.setText(ans7_3);
                String ans7_4 = answers7[3];
                button4.setText(ans7_4);
                return;
            case 7:
                String ans8_1 = answers8[0];
                button1.setText(ans8_1);
                String ans8_2 = answers8[1];
                button2.setText(ans8_2);
                String ans8_3 = answers8[2];
                button3.setText(ans8_3);
                String ans8_4 = answers8[3];
                button4.setText(ans8_4);
                return;
            case 8:
                String ans9_1 = answers9[0];
                button1.setText(ans9_1);
                String ans9_2 = answers9[1];
                button2.setText(ans9_2);
                String ans9_3 = answers9[2];
                button3.setText(ans9_3);
                String ans9_4 = answers9[3];
                button4.setText(ans9_4);
                return;
            case 9:
                String ans10_1 = answers10[0];
                button1.setText(ans10_1);
                String ans10_2 = answers10[1];
                button2.setText(ans10_2);
                String ans10_3 = answers10[2];
                button3.setText(ans10_3);
                String ans10_4 = answers10[3];
                button4.setText(ans10_4);
                return;
        }
    }

}